package com.zut.edu.app.history;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.zut.edu.app.R;

public class JieQiLiShiActivity extends Activity {
    TextView titleTv1,titleTv2,descTv;
    ImageView backIv,bigPicIv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history1);
        initView();
//        接受上一级页面传来的数据
        Intent intent = getIntent();
        HistoryBean historyBean = (HistoryBean) intent.getSerializableExtra("history");
//        设置显示控件
        titleTv1.setText(historyBean.getTitle());
        titleTv2.setText(historyBean.getTitle());
        descTv.setText(historyBean.getDesc());
        bigPicIv.setImageResource(historyBean.getId());
    }
    private void initView() {
        titleTv1 = findViewById(R.id.h_tv_title1);
        titleTv2 = findViewById(R.id.h_tv_title2);
        descTv = findViewById(R.id.h_tv_desc);
        backIv = findViewById(R.id.h_iv_back);
        bigPicIv = findViewById(R.id.h_iv_pic);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();   //销毁当前的activity
            }
        });
    }
}

