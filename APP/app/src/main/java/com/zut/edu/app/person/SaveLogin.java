package com.zut.edu.app.person;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SaveLogin {

    public static boolean saveUserInfo(Context context, String account,
                                       String password) {
        SharedPreferences sp = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("userName", account);
        edit.putString("pwd", password);
        edit.commit();
        return true;
    }
    public static boolean savePersonInfo(Context context, String username,  String usersex, String userbirth , String usernumber, String useremail) {
        SharedPreferences sp = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.putString("userRealName", username);
        edit.putString("userSex", usersex);
        edit.putString("userBirth", userbirth);
        edit.putString("userNumber", usernumber);
        edit.putString("userEmail", useremail);
        edit.commit();
        return true;
    }

    public static Map<String, String> getUserInfo(Context context) {
        SharedPreferences sp = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        String account = sp.getString("userName", null);
        String password = sp.getString("pwd", null);
        Map<String, String> userMap = new HashMap<String, String>();
        userMap.put("account", account);
        userMap.put("password", password);
        return userMap;
    }
    public static Map<String, String>  getPersonInfo(Context context) {
        SharedPreferences sp = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        String username = sp.getString("userRealName", null);
        String usersex = sp.getString("userSex", null);
        String userbirth = sp.getString("userBirth", null);
        String usernumber= sp.getString("userNumber", null);
        String useremail = sp.getString("userEmail", null);

        Map<String, String>  userMap = new HashMap<String, String> ();
        userMap.put("username", username);
        userMap.put("usersex",usersex);
        userMap.put("userbirth", userbirth);
        userMap.put("usernumber",usernumber);
        userMap.put("useremail", useremail);
        return userMap;
    }
}
