package com.zut.edu.app.person;


import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.zut.edu.app.R;
import com.zut.edu.app.person.Login;

import java.util.HashMap;
import java.util.Map;

public class PersonanlCenter extends Fragment  {
    TextView user,personinfo, personnote;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.frag_personalcenter,null);
        View v1=inflater.inflate(R.layout.activity_personinfo,null);
        user= v.findViewById(R.id.user_textView);
       personinfo= v.findViewById(R.id.textView2);
       personnote=v.findViewById(R.id.textView3);

        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it=new Intent(getActivity(),Login.class);
                startActivity(it);
            }
        });

        personinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it=new Intent(getActivity(),PersonInfoActivity.class);
                startActivity(it);
            }
        });
        personnote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it=new Intent(getActivity(),PersonNoteActivity.class);
                startActivity(it);
            }
        });
        return v;
    }
    public void initView(){

            SharedPreferences sp = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
            String account = sp.getString("userName", "NoValue");
            Log.d("userName",account);
            if(!account.equals("NoValue")){
            user.setText(account);

        }

    }
    public void jumplogin(View v){
        TextView login=(TextView) v.findViewById(R.id. textView );
        login.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
// TODO Auto-generated method stub
                Intent it=new Intent(getActivity(),Login.class);
                startActivity(it);
            }
        });
    }

    @Override
    public void onResume() {
        initView();
        super.onResume();
    }
}
