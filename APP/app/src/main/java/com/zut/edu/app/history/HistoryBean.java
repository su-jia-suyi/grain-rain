package com.zut.edu.app.history;

import java.io.Serializable;

public class HistoryBean implements Serializable {
    private String title;
    private String desc;
    private int Id;

    public HistoryBean() {
    }

    public HistoryBean(String title, String desc, int id) {
        this.title = title;
        this.desc = desc;
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

}
