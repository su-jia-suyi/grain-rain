package com.zut.edu.app.history;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.zut.edu.app.R;
import java.util.List;
public class HAdapter extends BaseAdapter {
    Context context;
    List<HistoryBean> mDatas;

    public HAdapter(Context context, List<HistoryBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
    }

    @Override
    public int getCount() {
        return mDatas.size();
    }

    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

//        1.声明ViewHolder
        HAdapter.ViewHolder holder = null;
        if (convertView == null) { //2.判断是否有复用的view，如果没有就创建
            convertView = LayoutInflater.from(context).inflate(R.layout.item_jieqi_history,null);
            holder = new HAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (HAdapter.ViewHolder) convertView.getTag();
        }
//        获取指定位置的数据
        HistoryBean healthBean = mDatas.get(position);
        holder.iv.setImageResource(healthBean.getId());
        holder.tv.setText(healthBean.getTitle());
        return convertView;
    }
    class ViewHolder{
        ImageView iv;
        TextView tv;
        public ViewHolder(View view){
            iv = view.findViewById(R.id.item_his_iv);
            tv = view.findViewById(R.id.item_his_tv);
        }
    }
}