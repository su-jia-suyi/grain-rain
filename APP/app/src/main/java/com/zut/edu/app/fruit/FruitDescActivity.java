package com.zut.edu.app.fruit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.zut.edu.app.R;

public class FruitDescActivity extends AppCompatActivity {
    TextView titleTv1,titleTv2,descTv;
    ImageView backIv,bigPicIv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit_desc);
        initView();
//        接受上一级页面传来的数据
        Intent intent = getIntent();
        FruitBean fruitBean = (FruitBean) intent.getSerializableExtra("food");
//        设置显示控件
        titleTv1.setText(fruitBean.getTitle());
        titleTv2.setText(fruitBean.getTitle());
        descTv.setText(fruitBean.getDesc());
        bigPicIv.setImageResource(fruitBean.getPicId());
    }
    private void initView() {
        titleTv1 = findViewById(R.id.fruitdesc_tv_title1);
        titleTv2 = findViewById(R.id.fruitdesc_tv_title2);
        descTv = findViewById(R.id.fruitdesc_tv_desc);
        backIv = findViewById(R.id.fruitdesc_iv_back);
        bigPicIv = findViewById(R.id.fruitdesc_iv_bigpic);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();   //销毁当前的activity
            }
        });
    }
}