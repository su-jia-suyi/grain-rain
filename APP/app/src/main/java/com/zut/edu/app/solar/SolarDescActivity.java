package com.zut.edu.app.solar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zut.edu.app.R;

public class SolarDescActivity extends AppCompatActivity {
    TextView name1, name2, introduction;
    ImageView back, bigPicIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solar_desc);
        initView();
//        接受上一级页面传来的数据
        Intent intent = getIntent();
        SolarBean solarBean = (SolarBean) intent.getSerializableExtra("solar");
//        设置显示控件
        name1.setText(solarBean.getName());
        name2.setText(solarBean.getName());
        introduction.setText(solarBean.getIntroduction());
        bigPicIv.setImageResource(solarBean.getPicId());
    }

    private void initView() {
        name1 = findViewById(R.id.solar_desc_tv_title1);
        name2 = findViewById(R.id.solar_desc_tv_title2);
        introduction = findViewById(R.id.solar_desc_tv_desc);
        back = findViewById(R.id.solar_desc_iv_back);
        bigPicIv = findViewById(R.id.solar_desc_iv_pic);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();   //销毁当前的activity
            }
        });
    }
}