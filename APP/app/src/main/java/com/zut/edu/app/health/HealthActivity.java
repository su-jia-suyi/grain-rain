package com.zut.edu.app.health;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.zut.edu.app.R;
import com.zut.edu.app.health.HealthBean;
import com.zut.edu.app.health.HealthDescActivity;
import com.zut.edu.app.health.HealthGridAdapter;
import com.zut.edu.app.health.HealthUtils;

import java.util.List;

public class HealthActivity extends Activity {


    GridView gv;
    List<HealthBean> mDatas;
    private HealthGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health);
        gv = findViewById(R.id.gv);
//        数据源
        mDatas = HealthUtils.getHealthList();
//        创建适配器对象
        adapter = new HealthGridAdapter(this, mDatas);
//        设置适配器
        gv.setAdapter(adapter);
        setListener();
    }

    private void setListener() {
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HealthBean healthBean = mDatas.get(position);
                Intent intent = new Intent(HealthActivity.this, HealthDescActivity.class);
                intent.putExtra("health",healthBean);
                startActivity(intent);
            }
        });
    }
}