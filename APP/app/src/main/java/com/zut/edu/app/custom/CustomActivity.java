package com.zut.edu.app.custom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.zut.edu.app.R;
import com.zut.edu.app.custom.CustomBean;
import com.zut.edu.app.custom.CustomDescActivity;
import com.zut.edu.app.custom.CustomGridAdapter;
import com.zut.edu.app.custom.CustomUtils;

import java.util.List;

public class CustomActivity extends Activity {


    GridView gv;
    List<CustomBean> mDatas;
    private CustomGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        gv = findViewById(R.id.gv);
//        数据源
        mDatas = CustomUtils.getAllCustomsList();
//        创建适配器对象
        adapter = new CustomGridAdapter(this, mDatas);
//        设置适配器
        gv.setAdapter(adapter);
        setListener();
    }

    private void setListener() {
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CustomBean customBean = mDatas.get(position);
                Intent intent = new Intent(com.zut.edu.app.custom.CustomActivity.this, CustomDescActivity.class);
                intent.putExtra("customs",customBean);
                startActivity(intent);
            }
        });
    }
}