package com.zut.edu.app.history;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.zut.edu.app.R;
import com.zut.edu.app.custom.CustomBean;
import com.zut.edu.app.custom.CustomDescActivity;
import com.zut.edu.app.custom.CustomGridAdapter;
import com.zut.edu.app.custom.CustomUtils;

import java.util.List;

public class HistoryActivity extends Activity {


    GridView gv;
    List<HistoryBean> mDatas;
    private HAdapter adapter;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        gv = findViewById(R.id.gv);

        mDatas = HistoryUtils.getAllHistoryList();

        adapter = new HAdapter(this,mDatas);

        gv.setAdapter(adapter);
        setListener();



    }

    private void setListener() {
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HistoryBean h = mDatas.get(position);
                Intent intent = new Intent(HistoryActivity.this, JieQiLiShiActivity.class);
                intent.putExtra("history",h);
                startActivity(intent);


            }
        });
    }
}