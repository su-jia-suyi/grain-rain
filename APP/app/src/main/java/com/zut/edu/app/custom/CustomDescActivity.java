package com.zut.edu.app.custom;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zut.edu.app.R;
import com.zut.edu.app.custom.CustomBean;

public class CustomDescActivity extends AppCompatActivity {
    TextView titleTv1,titleTv2,descTv;
    ImageView backIv,bigPicIv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_desc);
        initView();
//        接受上一级页面传来的数据
        Intent intent = getIntent();
        CustomBean customBean = (CustomBean) intent.getSerializableExtra("customs");
//        设置显示控件
        titleTv1.setText(customBean.getTitle());
        titleTv2.setText(customBean.getTitle());
        descTv.setText(customBean.getDesc());
        bigPicIv.setImageResource(customBean.getPicId());
    }
    private void initView() {
        titleTv1 = findViewById(R.id.customdesc_tv_title1);
        titleTv2 = findViewById(R.id.customdesc_tv_title2);
        descTv = findViewById(R.id.customdesc_tv_desc);
        backIv = findViewById(R.id.customdesc_iv_back);
        bigPicIv = findViewById(R.id.customdesc_iv_bigpic);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();   //销毁当前的activity
            }
        });
    }
}
