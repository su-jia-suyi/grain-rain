package com.zut.edu.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.zut.edu.app.custom.CustomActivity;
import com.zut.edu.app.fruit.FruitActivity;
import com.zut.edu.app.health.HealthActivity;
import com.zut.edu.app.history.HistoryActivity;
import com.zut.edu.app.person.PersonActivity;
import com.zut.edu.app.poetry.PoetryActivity;
import com.zut.edu.app.solar.SolarActivity;

public class MainActivity extends AppCompatActivity {

    Button button, button1, edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button query = (Button) findViewById(R.id.query);
        Button fruit = (Button) findViewById(R.id.fruit);
        Button person = (Button) findViewById(R.id.person);
        Button custom = (Button) findViewById(R.id.custom);
        Button poetry = (Button) findViewById(R.id.poetry);
        Button health = (Button) findViewById(R.id.health);
        Button history = (Button) findViewById(R.id.history);
        query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SolarActivity.class);
                startActivity(intent);
            }
        });

        fruit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FruitActivity.class);
                startActivity(intent);
            }
        });
        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PersonActivity.class);
                startActivity(intent);
            }
        });
        custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CustomActivity.class);
                startActivity(intent);
            }
        });
        health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HealthActivity.class);
                startActivity(intent);
            }
        });
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
        poetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PoetryActivity.class);
                startActivity(intent);
            }
        });

    }
}