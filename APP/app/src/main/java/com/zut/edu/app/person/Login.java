package com.zut.edu.app.person;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zut.edu.app.R;

import java.util.Map;


public class Login extends AppCompatActivity implements View.OnClickListener{
    ProgressDialog dialog;
    EditText et_userName, et_password;
    Button btn_login;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        Map<String, String> userInfo = SaveLogin.getUserInfo(this);
        if (userInfo != null) {
            et_userName.setText(userInfo.get("account"));
            et_password.setText(userInfo.get("password"));
        }
    }


    public void init() {
        et_userName = findViewById(R.id.Login_editText1);
        et_password = findViewById(R.id.Login_editText2);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:

                String account = et_userName.getText().toString().trim();
                String password = et_password.getText().toString();
                if (TextUtils.isEmpty(account)) {
                    Toast.makeText(this, "请输入用户名", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(this, "请输入密码", Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog = new ProgressDialog(Login.this);
                dialog.setTitle("提示");
                dialog.setMessage("正在登录，请等待.......");
                dialog.setCancelable(false);
                dialog.show();
                Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();

                boolean isSaveSuccess = SaveLogin.saveUserInfo(this, account, password);
                if (isSaveSuccess) {
                    Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
                    Intent it=new Intent(Login.this, PersonActivity.class);
                    startActivity(it);
                } else {
                    Toast.makeText(this, "保存失败", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }


    }

