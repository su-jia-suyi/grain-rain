package com.zut.edu.app.fruit;

import java.io.Serializable;

public class FruitBean implements Serializable {
    private String title;
    private String desc;
    private int picId;

    public FruitBean(String title, String desc, int picId) {
        this.title = title;
        this.desc = desc;
        this.picId = picId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getPicId() {
        return picId;
    }

    public void setPicId(int picId) {
        this.picId = picId;
    }

    public FruitBean() {
    }
}
