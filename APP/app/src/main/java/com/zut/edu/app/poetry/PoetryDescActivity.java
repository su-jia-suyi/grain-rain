package com.zut.edu.app.poetry;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zut.edu.app.R;


public class PoetryDescActivity extends AppCompatActivity {
    TextView titleTv1,titleTv2,descTv;
    ImageView backIv,bigPicIv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poetry_desc);
        initView();
//        接受上一级页面传来的数据
        Intent intent = getIntent();
        PoetryBean poetryBean = (PoetryBean) intent.getSerializableExtra("poem");
//        设置显示控件
        titleTv1.setText(poetryBean.getTitle());
        titleTv2.setText(poetryBean.getTitle());
        descTv.setText(poetryBean.getDesc());
        bigPicIv.setImageResource(poetryBean.getPicId());
    }
    private void initView() {
        titleTv1 = findViewById(R.id.poetrydesc_tv_title1);
        titleTv2 = findViewById(R.id.poetrydesc_tv_title2);
        descTv = findViewById(R.id.poetrydesc_tv_desc);
        backIv = findViewById(R.id.poetrydesc_iv_back);
        bigPicIv = findViewById(R.id.poetrydesc_iv_bigpic);
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();   //销毁当前的activity
            }
        });
    }
}