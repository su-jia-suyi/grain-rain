package com.zut.edu.app.person;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.zut.edu.app.R;

import java.util.Map;

public class PersonInfoActivity  extends AppCompatActivity implements View.OnClickListener{
    EditText name, sex,birth,number,email;
    Button personinfo_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personinfo);
        initView();
        Map<String, String> personinfo = SaveLogin.getPersonInfo(this);
        if (personinfo != null) {
            name.setText(personinfo.get("username"));
            sex.setText(personinfo.get("usersex"));
           birth.setText(personinfo.get("userbirth"));
          number.setText(personinfo.get("usernumber"));
            email.setText(personinfo.get("useremail"));
        }
    }

    private void initView() {
        name= (EditText) findViewById(R.id.name);
        sex = (EditText) findViewById(R.id.sex);
        birth = (EditText) findViewById(R.id.birth);
        number= (EditText) findViewById(R.id.number);
        email= (EditText) findViewById(R.id.email);
        personinfo_button = (Button) findViewById(R.id.personinfo_button);
        personinfo_button.setOnClickListener(this);
//

    }
    public void onClick(View v1) {
        switch (v1.getId()) {
            case R.id.personinfo_button:
                String username = name.getText().toString().trim();
                String usersex = sex.getText().toString();
                String userbirth = birth.getText().toString();
                String usernumber = number.getText().toString();
                String useremail = email.getText().toString();


                if (TextUtils.isEmpty(username)) {
                    Toast.makeText(this, "请输入姓名", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(usersex)) {
                    Toast.makeText(this, "请输入性别", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(userbirth)) {
                    Toast.makeText(this, "请输入生日", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(usernumber)) {
                    Toast.makeText(this, "请输入电话号码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(useremail)) {
                    Toast.makeText(this, "请输入电子邮箱", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(this, "提交成功", Toast.LENGTH_SHORT).show();

                boolean isSaveSuccess = SaveLogin.savePersonInfo(this, username, usersex, userbirth, usernumber, useremail);
                if (isSaveSuccess) {
                    Toast.makeText(this, "保存成功", Toast.LENGTH_SHORT).show();
//            Intent it = new Intent(PersonInfoActivity.this, PersonInfoActivity.class);
//            startActivity(it);
                } else {
                    Toast.makeText(this, "保存失败", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }}
