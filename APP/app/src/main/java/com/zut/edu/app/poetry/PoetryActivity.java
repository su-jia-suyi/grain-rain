package com.zut.edu.app.poetry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import android.view.View;
import android.widget.AdapterView;

import com.zut.edu.app.R;

import java.util.List;

public class PoetryActivity extends Activity {


    GridView gv;
    List<PoetryBean> mDatas;
    private PoetryGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poetry);
        gv = findViewById(R.id.gv);
//        数据源
        mDatas = PoetryUtils.getAllPoetryList();
//        创建适配器对象
        adapter = new PoetryGridAdapter(this, mDatas);
//        设置适配器
        gv.setAdapter(adapter);
        setListener();
    }


    private void setListener() {
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PoetryBean poetryBean = mDatas.get(position);
                Intent intent = new Intent(PoetryActivity.this, PoetryDescActivity.class);
                intent.putExtra("poem",poetryBean);
                startActivity(intent);
            }
        });
    }
}