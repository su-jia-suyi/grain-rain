package com.zut.edu.app.solar;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zut.edu.app.R;

import java.util.List;

public class SolarListAdapter extends BaseAdapter{
    Context context;
    List<SolarBean> mDatas;

    public SolarListAdapter(Context context, List<SolarBean> mDatas) {
        this.context = context;
        this.mDatas = mDatas;
    }
    //  决定了ListView列表展示的行数
    @Override
    public int getCount() {
        return mDatas.size();
    }
    //返回指定位置对应的数据
    @Override
    public Object getItem(int position) {
        return mDatas.get(position);
    }
    // 返回指定位置所对应的id
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_infolist_lv,null); //将布局转换成view对象的方法
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
//        加载控件显示的内容
//        获取集合指定位置的数据
        SolarBean solarBean = mDatas.get(position);
        holder.nameTv.setText(solarBean.getName());
        holder.iv.setImageResource(solarBean.getPicId());
        return convertView;
    }

    class ViewHolder{
        ImageView iv;
        TextView nameTv,intTv;
        public ViewHolder(View view){
            iv = view.findViewById(R.id.item_info_iv);
            nameTv = view.findViewById(R.id.item_info_tv_name);
            intTv = view.findViewById(R.id.item_info_tv_introduction);
        }
    }
}
