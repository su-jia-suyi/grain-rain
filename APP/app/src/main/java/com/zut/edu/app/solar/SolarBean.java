package com.zut.edu.app.solar;

import java.io.Serializable;

public class SolarBean implements Serializable {
    private String name;
    private String introduction;
    private int picId;

    public SolarBean(String name, String introduction, int picId) {
        this.name = name;
        this.introduction = introduction;
        this.picId = picId;
    }

    public SolarBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public int getPicId() {
        return picId;
    }

    public void setPicId(int picId) {
        this.picId = picId;
    }
}
