package com.zut.edu.app.fruit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.zut.edu.app.R;

import java.util.List;

public class FruitActivity extends Activity {


    GridView gv;
    List<FruitBean> mDatas;
    private FruitGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit);
        gv = findViewById(R.id.gv);
//        数据源
        mDatas = FruitUtils.getAllFoodList();
//        创建适配器对象
        adapter = new FruitGridAdapter(this, mDatas);
//        设置适配器
        gv.setAdapter(adapter);
        setListener();
    }

    private void setListener() {
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FruitBean fruitBean = mDatas.get(position);
                Intent intent = new Intent(FruitActivity.this, FruitDescActivity.class);
                intent.putExtra("food",fruitBean);
                startActivity(intent);
            }
        });
    }
}